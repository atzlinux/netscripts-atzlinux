铜豌豆网络小脚本软件包

Linux 命令行功能强大，参数多，对于一些非常常用的功能，
有时候也需要输入较多的字符。

这个软件包里面，将一些非常常用的命令，用简单的脚本，
实现一个常用的功能，不需要输入任何参数，
做到简单好用好记。

目前已经实现的小脚本命令如下：

- lip, lanip, localip - show LAN local IP of gateway device used by this machine
- lipv6, localipv6 - show LAN local IPv6 of gateway device used by this machine
- wip, wanip - show Internet public IP of this machine
- gw - show gateway device name use by this machine
- gwip - show default gateway IP
- pgw - ping default gateway IP
- gwmac - show gateway device mac address
- iftopg, iftopgw - display bandwidth usage on gateway device
- iftopb, iftopbluetooth - display bandwidth usage on first bluetooth device
- dns - show /etc/resolv.conf

该软件包由 铜豌豆 Linux 项目 https://www.atzlinux.com 制作，
已经制作为 deb 格式的软件包放入铜豌豆软件源

https://www.atzlinux.com/allpackages.htm

软件包名为：

netscripts-atzlinux

AUR 也有由网友帮忙制作的软件包。

## Packaging status

[![Packaging status](https://repology.org/badge/vertical-allrepos/netscripts-atzlinux.svg)](https://repology.org/project/netscripts-atzlinux/versions)

项目 git 仓库：
https://gitee.com/atzlinux/netscripts-atzlinux

欢迎贡献自己的小脚本，具体请参阅： README.devel.txt
