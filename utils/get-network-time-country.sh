#!/bin/bash
# 
# version:1.2
# date:2018-07-13
# author:shiye.meng faris.xiao
# copyright: GPL2
# changelog:
# v1.1:whois grep use head -n 1 only
# v1.2:add ip number filter 
#      add country name display
#      change whois grep use to tail -n 1
#
StartTime="`date`"
NDate="`date +%Y%m%d%H%M`"
LogDir="logs/${NDate}"
#
if [ ! -e ip.txt ]
 then
  echo " ip.txt not found"
  exit 1
fi

#diff ip
dos2unix ip.txt
cat ip.txt|grep -v ^$|grep ^[0-9]|grep [0-9]$|sort -u > tmp.txt
mv tmp.txt ip.txt
IPN=`wc -l ip.txt`
echo "IP number is $IPN"

#
if [ ! -d "logs/${NDate}" ]
 then
  mkdir -pv logs/${NDate}
fi
#rm logsfile
rm -f logs/${NDate}/*

#step 1
#traceroute ip msg
echo "begin traceroute"
while read ip
do
sleep 0.01
traceroute -n $ip > ${LogDir}/$ip.tinfo &
done < ip.txt

echo "running 30 secends"
for i in `seq 1 30`;do echo -n ".";sleep 1;done

#exit 
#step 2
#get last traceroute time
#for i in `cat ip.txt`;do echo -n $i;echo -n -e "\t";grep ms ${LogDir}/$i.tinfo|tail -n 1|sed s/*//g|awk {'print $3'} ;done > ${LogDir}/speed.info
echo >${LogDir}/speed.info
echo
echo "begin get net time info"
for i in `cat ip.txt`
do
echo "$i `grep ms ${LogDir}/$i.tinfo|tail -n 1|sed s/*//g|awk {'print $3'}`" >>${LogDir}/speed.info
done

#exit
#exit 
#step 3
#get country
echo "begin get country info:"

while read ip
do
whois $ip > ${LogDir}/$ip.winfo &
done < ip.txt

echo "running 10 secends"
for i in `seq 1 10`;do echo -n ".";sleep 1;done

echo > ${LogDir}/country.txt
while read ip
do
#echo "$ip `grep -iE \"country\" ${LogDir}/$ip.winfo|head -n 1|awk '{print $2}'` ">>${LogDir}/country.txt 
echo "$ip `grep -iE \"country\" ${LogDir}/$ip.winfo|tail -n 1|awk -F":" {'print $NF'}` ">>${LogDir}/country.txt 
done < ip.txt

# del space line 
grep -v ^$ ${LogDir}/country.txt > ${LogDir}/a.tmp
rm -f ${LogDir}/country.txt
mv ${LogDir}/a.tmp ${LogDir}/country.txt
#wc -l ${LogDir}/country.txt

#step 4
#get match line
cat ${LogDir}/country.txt |awk '{if(NF >1)print $0}' > ${LogDir}/c.txt
#wc -l ${LogDir}/country.txt
#wc -l ${LogDir}/c.txt

echo "can't get country info"
diff ${LogDir}/c.txt ${LogDir}/country.txt

# add NONE to can't get country IP
cat ${LogDir}/country.txt |awk '{if(NF == 1)print $0"NONE"}' >> ${LogDir}/c.txt
#wc -l ${LogDir}/country.txt
#wc -l ${LogDir}/c.txt

# no country info ip
echo "no country info ip"
grep NONE ${LogDir}/c.txt|awk {'print $1'} > NONE.country.ip
wc -l NONE.country.ip
cat NONE.country.ip

#exit 
#step 5
#get ip country times
echo >${LogDir}/ip_country_time.txt
while read ip
do
echo "$ip `grep $ip ${LogDir}/c.txt ${LogDir}/speed.info |xargs|awk '{print $2,$4}'`" >> ${LogDir}/ip_country_time.txt
done < ip.txt

# del space line 
grep -v ^$ ${LogDir}/ip_country_time.txt > ${LogDir}/a.tmp
rm -f ${LogDir}/ip_country_time.txt
mv ${LogDir}/a.tmp ${LogDir}/ip_country_time.txt

#step 6
#get msg
awk '{a[$2]+=1;b[$2]+=$3}END{for(i in a)print i,a[i],b[i]/a[i]}' ${LogDir}/ip_country_time.txt |sed -n '1,$p' >country_num_avg.txt

#get country name
CCDATA="/usr/local/share/data/code-country.txt"
while read country number time
do
echo -n "$country";echo -n -e "\t""$number";echo -n -e "\t"$time;echo -e "\t" `grep -i "^$country " $CCDATA |awk {'print $2'}`
done < country_num_avg.txt >  country_num_avg_name.txt

#step 7
#final
sort country_num_avg_name.txt > ${LogDir}/sort-country_num_avg_name.txt
cp ${LogDir}/sort-country_num_avg_name.txt country_num_avg_name.txt

sed -i '1icountry ips avg_ms' country_num_avg.txt
sed -i '1icountry ips avg_ms name' country_num_avg_name.txt

#
#end time
echo "From [${StartTime}] to `date`"

#show
echo
echo
cat country_num_avg_name.txt
echo 
#cat country_num_avg.txt

exit
