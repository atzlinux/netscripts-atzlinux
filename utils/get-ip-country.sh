#!/bin/bash
# 
# version:1.0
# date:2018-08-16
# author:shiye.meng faris.xiao
# copyright: GPL2
# changelog:
# v1.1:whois grep use head -n 1 only
# v1.2:add ip number filter 
#      add country name display
#      change whois grep use to tail -n 1
#
StartTime="`date`"
NDate="`date +%Y%m%d%H%M`"
LogDir="logs/${NDate}"
#
if [ ! -e ip.txt ]
 then
  echo " ip.txt not found"
  exit 1
fi

#diff ip
dos2unix ip.txt
cat ip.txt|grep -v ^$|grep ^[0-9]|grep [0-9]$|sort -u > tmp.txt
mv tmp.txt ip.txt
IPN=`wc -l ip.txt`
echo "IP number is $IPN"

#
if [ ! -d "logs/${NDate}" ]
 then
  mkdir -pv logs/${NDate}
fi
#rm logsfile
rm -f logs/${NDate}/*

#get country
echo "begin get country info:"

while read ip
do
whois $ip > ${LogDir}/$ip.winfo &
sleep 1
done < ip.txt

echo "running 10 secends"
for i in `seq 1 10`;do echo -n ".";sleep 1;done

echo > ${LogDir}/country.txt
while read ip
do
#echo "$ip `grep -iE \"country\" ${LogDir}/$ip.winfo|head -n 1|awk '{print $2}'` ">>${LogDir}/country.txt 
echo "$ip `grep -iE \"country\" ${LogDir}/$ip.winfo|tail -n 1|awk -F":" {'print $NF'}` ">>${LogDir}/country.txt 
done < ip.txt

# del space line 
grep -v ^$ ${LogDir}/country.txt > ${LogDir}/a.tmp
rm -f ${LogDir}/country.txt
mv ${LogDir}/a.tmp ${LogDir}/country.txt
#wc -l ${LogDir}/country.txt

#step 4
#get match line
cat ${LogDir}/country.txt |awk '{if(NF >1)print $0}' > ${LogDir}/c.txt
#wc -l ${LogDir}/country.txt
#wc -l ${LogDir}/c.txt

echo "can't get country info"
diff ${LogDir}/c.txt ${LogDir}/country.txt

# add NONE to can't get country IP
cat ${LogDir}/country.txt |awk '{if(NF == 1)print $0"NONE"}' >> ${LogDir}/c.txt
#wc -l ${LogDir}/country.txt
#wc -l ${LogDir}/c.txt

# no country info ip
echo "no country info ip"
grep NONE ${LogDir}/c.txt|awk {'print $1'} > NONE.country.ip
wc -l NONE.country.ip
cat NONE.country.ip
wc -l NONE.country.ip
